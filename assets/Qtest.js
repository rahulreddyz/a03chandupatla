QUnit.test('Testing Multiplication function with four sets of inputs', function (assert) {
    assert.throws(function () { multiply(); }, new Error("only numbers are allowed"), 'Passing in array correctly raises an Error');
    assert.strictEqual(multiply(3,4), 12, 'All positive numbers');
    assert.strictEqual(multiply(3,-7), -21, 'Positive and negative numbers');
    assert.strictEqual(multiply(-5,-8), 40, 'All are negative numbers');
});
